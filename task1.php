<?php

/**
 * Осуществляем рекурсивный поиск всех файлов count и суммируем значение из этого файла
 * Так как обходим все дерево полностью, то не важно в глубину или в ширину
 * @param $startPath
 * @return float|int|mixed
 */
function sumCountFileAtDir_var1($startPath)
{
    $fName = __function__;
    $sum = 0;
    //echo $startPath . PHP_EOL;
    $DirElements = glob($startPath . '/*');

    foreach ($DirElements as $item) {
        if (is_dir($item)) {
            $sum += $fName($item);
            continue;
        }

        $pathByElements = explode('/', $item);
        $filename = end($pathByElements);
        if ($filename !== 'count') {
            continue;
        }

        $val = trim(file_get_contents($item));
        if (is_numeric($val)) {
            $sum += (float)trim(file_get_contents($item));
            echo "{$item} : {$sum}" . PHP_EOL;
        }
    }
    return $sum;
}

/**
 * Более выразительный вариант
 * @param $startPath
 * @return int
 */
function sumCountFileAtDir_var2($startPath)
{
    $sum = 0;
    $dirIterator = new RecursiveDirectoryIterator($startPath);
    $ite = new RecursiveIteratorIterator($dirIterator);

    /** @var $item SplFileInfo */
    foreach ($ite as $item) {
        if ($item->getFilename() !== 'count') {
            continue;
        }

        $val = trim(file_get_contents($item->getRealPath()));
        if (is_numeric($val)) {
            $sum += (float)trim(file_get_contents($item));
            echo "{$item} : {$sum}" . PHP_EOL;
        }
    }
    return $sum;
}


echo "Вариант 1:" . PHP_EOL;
echo sumCountFileAtDir_var1('/home/yak/Documents');
echo PHP_EOL . "=====================" . PHP_EOL;
echo "Вариант 2" . PHP_EOL;
echo sumCountFileAtDir_var2('/home/yak/Documents');

